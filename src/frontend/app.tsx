import 'reflect-metadata';
import React from 'react';
import ReactDOM from 'react-dom';

import App from './scenes';
import createApolloClient from './apollo/client';

async function bootstrap() {
  const client = await createApolloClient();
  client.writeData({
    data: {
      core: {
        __typename: 'Core',
        searchResultRowCount: 0,
      },
    },
  });
  ReactDOM.render(<App client={client} />, document.getElementById('root')!);
}

void (async () => {
  try {
    await bootstrap();
  } catch (e) {
    console.log(e);
  }
})();
