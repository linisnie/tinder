import ApolloClient from 'apollo-boost';
// @ts-ignore
import typeDefs from '../schema.graphql';

export default async function createApolloClient() {
  return new ApolloClient({
    typeDefs,
    resolvers: { Mutation: {}, Query: {} },
  });
}
