import React from 'react';
import { Link } from 'react-router-dom';
import Pagination from '@material-ui/lab/Pagination';
import PaginationItem from '@material-ui/lab/PaginationItem';
import { userUrlParamQuery } from './use-url-param-query';
import { useQuery } from 'react-apollo';
import { gql } from 'apollo-boost';
import { PAGE_SIZE } from '../../common/constants';
import queryString from 'query-string';
import { SearchUserInput } from '../../backend/db/resolvers/input-type';

export default function PaginationLink() {
  let query: SearchUserInput = userUrlParamQuery();
  const { data: coreData } = useQuery(
    gql`
      query searchResultRowCount {
        core @client {
          searchResultRowCount
        }
      }
    `,
  );

  return (
    <Pagination
      page={Number(query.page)}
      count={Math.ceil(coreData.core.searchResultRowCount / PAGE_SIZE)}
      size="large"
      renderItem={(item) => (
        <PaginationItem
          component={Link}
          to={`/?${queryString.stringify({ ...query, page: item.page })}`}
          {...item}
        />
      )}
    />
  );
}
