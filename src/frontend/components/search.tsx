import React, { useState } from 'react';
import {
  fade,
  makeStyles,
  Theme,
  createStyles,
} from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import { userUrlParamQuery } from './use-url-param-query';
import { useHistory } from 'react-router-dom';
import queryString from 'query-string';
import {SearchUserInput} from "../../backend/db/resolvers/input-type";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto',
      },
      maxWidth: 400,
      flexGrow: 1,
    },
    wrapper: {
      flexGrow: 1,
      display: 'flex',
      justifyContent: 'center',
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      color: 'inherit',
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '20ch',
      },
    },
  }),
);

let timer: ReturnType<typeof setTimeout>;
export default function Search() {
  const query: SearchUserInput = userUrlParamQuery();
  const [value, setValue] = useState(query.search || '');
  const classes = useStyles();
  const history = useHistory();
  function handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    const { value } = e.target;
    clearTimeout(timer);
    setValue(value);
    timer = setTimeout(() => {
      query.search = value;
      query.page = '1';
      history.push(`/?${queryString.stringify(query)}`);
    }, 300);
  }
  return (
    <div className={classes.wrapper}>
      <div className={classes.search}>
        <div className={classes.searchIcon}>
          <SearchIcon />
        </div>
        <InputBase
          placeholder="Search…"
          value={value}
          onChange={handleChange}
          classes={{
            root: classes.inputRoot,
            input: classes.inputInput,
          }}
          inputProps={{ 'aria-label': 'search' }}
        />
      </div>
    </div>

  );
}
