import queryString from 'query-string';
import { useLocation } from 'react-router-dom';

export function userUrlParamQuery() {
  return queryString.parse(useLocation().search);
}
