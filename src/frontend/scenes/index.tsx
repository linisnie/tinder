import React, { FC } from 'react';
import { ApolloProvider } from 'react-apollo';
import ApolloClient from 'apollo-boost';
import { Layout } from '../components/layout';
import { UsersContainer } from './user';
import { BrowserRouter as Router } from 'react-router-dom';

const App: FC<{ client: ApolloClient<any> }> = ({ client }) => {
  return (
    <ApolloProvider client={client}>
      <Router>
        <Layout>
          <UsersContainer />
        </Layout>
      </Router>
    </ApolloProvider>
  );
};

export default App;
