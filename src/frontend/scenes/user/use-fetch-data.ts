import { useQuery } from 'react-apollo';
import { Users as UserType } from '../../../backend/db/models/users';
import { gql } from 'apollo-boost';
import { SearchUserInput } from '../../../backend/db/resolvers/input-type';

export function useFetchData(query: SearchUserInput) {
  const { data, loading } = useQuery<{ users: UserType[] }>(
    gql`
      query users($params: SearchUserInput!) {
        users(params: $params) {
          name
          bio
          birth
          university
          job
          instagram
          height
          images {
            url
          }
        }
      }
    `,
    {
      variables: {
        params: {
          page: query.page || '1',
          search: query.search || '',
          university: query.university,
          hasInst: query.hasInst,
        } as SearchUserInput,
      },
    },
  );
  return { data, loading };
}
