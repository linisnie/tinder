import React, { FC, useRef, useState } from 'react';
import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';
import { Users as UserType } from '../../../../backend/db/models/users';
import SwipeableViews from 'react-swipeable-views';
import clsx from 'clsx';
import { isMobile } from '../../../core/get-device-type';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
  },
  slide: {
    padding: 15,
    minHeight: 100,
    color: '#fff',
  },
  slide1: {
    backgroundColor: '#FEA900',
  },
  slide2: {
    backgroundColor: '#B3DC4A',
  },
  slide3: {
    backgroundColor: '#6AC0FF',
  },
  cardMedia: {
    paddingTop: '100%',
  },
}));

export const ImageSlider: FC<{ user: UserType }> = ({ user }) => {
  const [index, setIndex] = useState(0);
  const divEl = useRef<HTMLDivElement>(null);
  const classes = useStyles();
  const imagesCount = (user.images && user.images.length) || 0;
  const handleMove = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    if (divEl.current === null) return;
    if (isMobile()) return;
    const x = e.pageX - divEl?.current.offsetLeft;
    const width = divEl?.current.offsetWidth;
    let newIndex = Math.ceil((x / width) * imagesCount) - 1;
    if (newIndex < 0) newIndex = 0;
    if (newIndex !== index) setIndex(newIndex);
  };
  return (
    <div ref={divEl} onMouseMove={handleMove} className={classes.root}>
      {imagesCount > 1 && <ActiveEl index={index} count={imagesCount} />}
      <SwipeableViews onChangeIndex={(i) => setIndex(i)} index={index}>
        {user.images &&
          user.images[0] &&
          user.images.map((image, i) => (
            <CardMedia
              key={i}
              className={classes.cardMedia}
              image={image.url}
            />
          ))}
      </SwipeableViews>
    </div>
  );
};

const useStylesActive = makeStyles((theme) => ({
  root: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: 5,
    width: '100%',
    display: 'flex',
    zIndex: 1,
    padding: '3px 4px',
  },
  el: {
    height: 3,
    backgroundColor: '#33333333',
    margin: 2,
    borderRadius: 3,
    flexGrow: 1,
  },
  active: {
    backgroundColor: '#fff',
  },
}));

export const ActiveEl: FC<{ index: number; count: number }> = ({
  index,
  count,
}) => {
  const classes = useStylesActive();
  return (
    <div className={classes.root}>
      {Array(count)
        .fill(1)
        .map((e, i) => (
          <div
            key={i}
            className={clsx(classes.el, i === index && classes.active)}
          />
        ))}
    </div>
  );
};
