import React, { FC } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import { SelectUniversity } from './university-select';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox, { CheckboxProps } from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import Favorite from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import { SearchUserInput } from '../../../../backend/db/resolvers/input-type';
import { userUrlParamQuery } from '../../../components/use-url-param-query';
import { useHistory } from 'react-router-dom';
import queryString from 'query-string';

export const drawerWidth = 340;

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-start',
  },
  checkbox: {
    margin: theme.spacing(2),
  },
}));

export const UserDrawer: FC<{
  open: boolean;
  handleDrawerClose: () => void;
}> = ({ open, handleDrawerClose }) => {
  const classes = useStyles();
  const theme = useTheme();
  let query: SearchUserInput = userUrlParamQuery();
  const history = useHistory();
  const setHasInst = () => {
    query.hasInst = query.hasInst === 'true' ? 'false' : 'true';
    history.push(`/?${queryString.stringify(query)}`);
  };

  return (
    <Drawer
      className={classes.drawer}
      variant="persistent"
      anchor="right"
      open={open}
      classes={{
        paper: classes.drawerPaper,
      }}>
      <div className={classes.drawerHeader}>
        <IconButton onClick={handleDrawerClose}>
          {theme.direction === 'rtl' ? (
            <ChevronLeftIcon />
          ) : (
            <ChevronRightIcon />
          )}
        </IconButton>
      </div>
      <Divider />
      <SelectUniversity />
      <Divider />
      <FormControlLabel
        className={classes.checkbox}
        control={
          <Checkbox
            checked={query.hasInst === 'true'}
            onChange={setHasInst}
            name="checkedA"
          />
        }
        label="Has instagram"
      />
    </Drawer>
  );
};
