import React, { FC } from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {
  createMuiTheme,
  makeStyles,
  ThemeProvider,
} from '@material-ui/core/styles';
import { Users as UserType } from '../../../../backend/db/models/users';
import { ImageSlider } from './image-slider';

const useStyles = makeStyles((theme) => ({
  card: {
    height: '100%',
    widows: 300,
    display: 'flex',
    flexDirection: 'column',
    transform: 'translateZ(0)',
  },
  cardMedia: {
    paddingTop: '100%',
  },
  cardContent: {
    flexGrow: 1,
  },
  pos: {
    marginBottom: 12,
  },
}));

const innerTheme = createMuiTheme({
  shape: { borderRadius: 8 },
  // @ts-ignore
  shadows: ['none', '0px 3px 30px 3px rgba(0,0,0,0.1)'],
});

export const ProfileCard: FC<{ user: UserType }> = ({ user }) => {
  const classes = useStyles();
  return (
    <ThemeProvider theme={innerTheme}>
      <Card elevation={1} className={classes.card}>
        {/*{user.images && user.images[0] && (*/}
        {/*  <CardMedia className={classes.cardMedia} image={user.images[0].url} />*/}
        {/*)}*/}
        <ImageSlider user={user} />
        <CardContent className={classes.cardContent}>
          <Typography gutterBottom variant="h5" component="h2">
            {user.name}
          </Typography>
          <Typography
            className={classes.pos}
            variant="body2"
            color="textSecondary">
            {user.university}
          </Typography>
          <Typography variant="body2">{user.bio}</Typography>
        </CardContent>
        <CardActions>
          {user.instagram && (
            <Button
              target="_blank"
              size="small"
              href={'https://instagram.com/' + user.instagram}
              color="primary">
              Inst
            </Button>
          )}
        </CardActions>
      </Card>
    </ThemeProvider>
  );
};
