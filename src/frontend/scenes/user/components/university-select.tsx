import React, { ChangeEvent, FC } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useQuery } from 'react-apollo';
import { gql } from 'apollo-boost';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { userUrlParamQuery } from '../../../components/use-url-param-query';
import { useHistory } from 'react-router-dom';
import queryString from 'query-string';
import { SearchUserInput } from '../../../../backend/db/resolvers/input-type';

const useStyles = makeStyles((theme) => ({}));

export const SelectUniversity: FC = () => {
  let query: SearchUserInput = userUrlParamQuery();
  const classes = useStyles();
  const { data } = useQuery<{ universities: string[] }>(
    gql`
      query universities {
        universities
      }
    `,
  );
  const history = useHistory();
  function handleChange(e: ChangeEvent<{}>, value: string | null) {
    if (!value) return;
    if (value === 'Not selected') value = '';
    query.university = value;
    query.page = '1';
    history.push(`/?${queryString.stringify(query)}`);
  }
  if (!data) return null;

  return (
    <Autocomplete
      id="combo-box-demo"
      options={['Not selected', ...data?.universities]}
      getOptionLabel={(el) => el}
      value={query.university}
      onChange={handleChange}
      style={{ width: 300, margin: 16 }}
      renderInput={(params) => <TextField {...params} label="University" />}
    />
  );
};
