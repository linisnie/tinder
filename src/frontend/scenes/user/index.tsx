import React, { FC } from 'react';
import Grid from '@material-ui/core/Grid';
import { ProfileCard } from './components/profile-card';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import PaginationLink from '../../components/pagination';
import { userUrlParamQuery } from '../../components/use-url-param-query';
import { useRowCount } from './use-row-count';
import { useFetchData } from './use-fetch-data';
import { SearchUserInput } from '../../../backend/db/resolvers/input-type';

export const UsersContainer: FC = () => {
  const classes = useStyles();
  let query: SearchUserInput = userUrlParamQuery();
  useRowCount(query);
  const { data, loading } = useFetchData(query);

  if (loading) return null;
  return (
    <Container className={classes.cardGrid} maxWidth="md">
      <Grid container spacing={4}>
        {data?.users.map((user, key, i) => (
          <Grid item key={key} xs={12} sm={6} md={4}>
            <ProfileCard user={user} />
          </Grid>
        ))}
      </Grid>
      <div className={classes.pagesContainer}>
        <PaginationLink />
      </div>
    </Container>
  );
};

const useStyles = makeStyles((theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  pagesContainer: {
    display: 'flex',
    justifyContent: 'center',
    margin: theme.spacing(4),
    marginTop: theme.spacing(8),
    marginBottom: 0,
  },
}));
