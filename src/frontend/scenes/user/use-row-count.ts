import { useApolloClient } from '@apollo/react-hooks';
import { useQuery } from 'react-apollo';
import { gql } from 'apollo-boost';
import { SearchUserInput } from '../../../backend/db/resolvers/input-type';

export function useRowCount(query: SearchUserInput) {
  const client = useApolloClient();
  useQuery(
    gql`
      query searchResultRowsCount($params: SearchUserInput!) {
        searchResultRowsCount(params: $params)
      }
    `,
    {
      variables: {
        params: {
          search: query.search || '',
          university: query.university,
          hasInst: query.hasInst,
        } as SearchUserInput,
      },
      onCompleted: (data) => {
        client.writeData({
          data: {
            core: {
              __typename: 'Core',
              searchResultRowCount: data.searchResultRowsCount,
            },
          },
        });
      },
    },
  );
}
