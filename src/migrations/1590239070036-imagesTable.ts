import {MigrationInterface, QueryRunner} from "typeorm";

export class imagesTable1590239070036 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`
            insert into user_image(user_id_user, url, file_name)
            with subq as (
                select id_user,
                       unnest((select array_agg(replace((json -> 'url')::text, '"', ''))
                               from json_array_elements(json -> 'user' -> 'photos') as x(json))) as url
                from "user"
            )
            select distinct id_user, url, (regexp_matches(url, '/([^/]+)$'))[1] as file_name
            from subq
            on conflict (url) do nothing
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`delete from user_image`);
    }

}
