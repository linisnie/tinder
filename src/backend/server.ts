import 'reflect-metadata';
import { buildSchema } from 'type-graphql';
import { UserResolver } from './db/resolvers/user';
import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import path from 'path';
import { createConnection } from 'typeorm';
import { dbParams } from './parser/constants';
import cookieParser from 'cookie-parser';

void (async function bootstrap() {
  await createConnection(dbParams);
  const schema = await buildSchema({
    resolvers: [UserResolver],
    emitSchemaFile: path.resolve(
      __dirname,
      '__snapshots__/schema/schema.graphql',
    ),
  });

  // Create GraphQL server
  const server = new ApolloServer({
    schema,
  });
  const app = express();
  const publicPath = path.join(__dirname, '../../build/public');
  app.use(express.static(publicPath));
  app.use(cookieParser());
  server.applyMiddleware({ app });
  const port = 5000;
  app.listen({ port }, () =>
    console.log(
      `🚀 Server ready at http://localhost:${port}${server.graphqlPath}`,
    ),
  );
})();
