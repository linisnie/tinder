import settings from '../../../settings.json';
import { Users } from '../db/models/users';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { UserImage } from '../db/models/user-image';
import { ImageAttributes } from '../db/models/image-attributes';
import {ConnectionOptions} from "typeorm";

const {
  postgres: POSTGRES,
  parser: {
    request: {
      APP_SESSION_ID,
      PERSISTENT_DEVICE_ID,
      USER_SESSION_ID,
      X_AUTH_TOKEN,
    },
    delay,
    limit,
    strategy,
  },
} = settings;

if (!['like', 'dislike'].includes(strategy))
  throw Error(
    `Strategy in settings.json should be 'like' or 'dislike', instead got '${strategy}'`,
  );

export const DELAY = delay;
export const STRATEGY = strategy;
export const LIMIT = limit;

export const commonHeaders = {
  'accept-language': 'en-US,en;q=0.9,ru;q=0.8',
  'app-session-id': APP_SESSION_ID,
  'app-session-time-elapsed': '501279',
  'app-version': '1023700',
  dnt: '1',
  origin: 'https://tinder.com',
  'persistent-device-id': PERSISTENT_DEVICE_ID,
  platform: 'web',
  referer: 'https://tinder.com/',
  'sec-fetch-dest': 'empty',
  'sec-fetch-mode': 'cors',
  'sec-fetch-site': 'cross-site',
  'tinder-version': '2.37.0',
  'user-agent':
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
  'user-session-id': USER_SESSION_ID,
  'user-session-time-elapsed': '501279',
  'x-auth-token': X_AUTH_TOKEN,
  'x-supported-image-formats': 'webp,jpeg',
};

export const dbParams: ConnectionOptions = {
  type: 'postgres',
  host: POSTGRES.host,
  port: POSTGRES.port,
  username: POSTGRES.username,
  password: POSTGRES.password,
  database: POSTGRES.database,
  entities: [Users, UserImage, ImageAttributes],
  synchronize: true,
  namingStrategy: new SnakeNamingStrategy(),
  cli: {
    migrationsDir: 'src/migrations',
  },
  // logger: 'advanced-console',
  // logging: 'all',
};
