import { userData } from './interface';
import { commonHeaders } from './constants';
import rp from 'request-promise';

interface objectType {
  [key: string]: any;
}

export function extractData(json: objectType) {
  const res: userData = {
    id: json.user._id,
    bio: json.user.bio,
    name: json.user.name,
    job: json.user.jobs[0]?.title?.name,
    company: json.user.jobs[0]?.company?.name,
    university: json.user.schools[0]?.name,
    birth: json.user.birth_date,
    distance: json.distance_mi,
    json: json,
    images: getImages(json),
    sNumber: json.s_number,
    photoId: json.user.photos[0].id,
  };
  return res;
}

function getImages(json: objectType) {
  return json.user.photos.map((photo: objectType) => photo.url);
}

export async function getData() {
  const options = {
    uri: 'https://api.gotinder.com/v2/recs/core?locale=en',
    headers: commonHeaders,
  };
  let res;
  try {
    res = await rp(options);
  } catch (e) {
    console.log(e);
  }
  return JSON.parse(res);
}

export async function like(userId: string, likedId: string, sNumber: number) {
  const options = {
    method: 'POST',
    uri: `https://api.gotinder.com/like/${userId}?locale=en`,
    headers: commonHeaders,
    body: {
      s_number: sNumber,
      liked_content_id: likedId,
      liked_content_type: 'photo',
    },
    json: true,
  };

  let res;
  try {
    res = await rp(options);
  } catch (e) {
    console.log(e);
  }
  return res;
}

export async function getShareLink(userId: string) {
  const options = {
    method: 'POST',
    uri: `https://api.gotinder.com/user/${userId}/share`,
    headers: commonHeaders,
    json: true,
  };

  let res;
  try {
    res = await rp(options);
  } catch (e) {
    console.log(e);
  }
  return res;
}

export async function dislike(userId: string, sNumber: number) {
  const options = {
    uri: `https://api.gotinder.com/pass/${userId}?locale=en&s_number=${sNumber}`,
    headers: commonHeaders,
  };

  let res;
  try {
    res = await rp(options);
  } catch (e) {
    console.log(e);
  }
  return JSON.parse(res);
}
