export interface userData {
  id: string;
  bio: string;
  name: string;
  birth: string;
  distance: number;
  json: { [key: string]: any };
  sNumber: number;
  photoId: string;
  university?: string;
  company?: string;
  job?: string;
  images: string[];
}
