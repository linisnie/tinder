import { Arg, Query, Resolver } from 'type-graphql';
import { Users } from '../models/users';
import { userData } from '../../parser/interface';
import { UserImage } from '../models/user-image';
import { SearchUserInput } from './input-type';
import { SelectQueryBuilder } from 'typeorm';
import { PAGE_SIZE } from '../../../common/constants';

@Resolver()
export class UserResolver {
  @Query(() => [Users])
  async users(@Arg('params') params: SearchUserInput) {
    const userIdsQuery = this.applyUserConditions(
      Users.createQueryBuilder('u'),
      params,
    );

    const userIds = await userIdsQuery
      .orderBy('u.id_user')
      .limit(PAGE_SIZE)
      .offset((Number(params.page) - 1) * PAGE_SIZE)
      .getMany();

    return Users.findByIds(userIds, {
      relations: ['images', 'images.attributes'],
    });
  }

  applyUserConditions(
    query: SelectQueryBuilder<Users>,
    params: SearchUserInput,
  ) {
    query.where('id_user notnull');
    if (params.search)
      query.andWhere('to_tsvector(u.bio) @@ to_tsquery(:search)', {
        search: params.search,
      });
    if (params.hasInst && params.hasInst === 'true')
      query.andWhere('instagram notnull', {
        search: params.search,
      });
    if (params.university)
      query.andWhere('u.university = :university', {
        university: params.university,
      });
    return query;
  }

  @Query(() => Number)
  searchResultRowsCount(@Arg('params') params: SearchUserInput) {
    return this.applyUserConditions(
      Users.createQueryBuilder('u'),
      params,
    ).getCount();
  }

  @Query(() => Users)
  user(@Arg('id') idUser: string) {
    return Users.findOne({
      where: { idUser },
      relations: ['images', 'images.attributes'],
    });
  }

  @Query(() => [String])
  async universities() {
    const u = await Users.createQueryBuilder()
      .select('university')
      .addSelect('count(*)', 'cnt')
      .where('university notnull')
      .andWhere(`university !~ '^[0-9.-]+'`)
      .groupBy('university')
      .having('count(*) > 10')
      .orderBy('1')
      .getRawMany();
    return u.map((el) => el.university);
  }
}

export async function uploadToDb(data: userData) {
  try {
    const images = await Promise.all(
      data.images.map((image) => {
        // @ts-ignore
        const [, fileName] = image.match(/\/([^/]+)$/);
        const userImage = UserImage.create({ url: image, fileName });
        return userImage.save();
      }),
    );
    const user = Users.create({
      id: data.id,
      bio: data.bio,
      birth: data.birth,
      name: data.name,
      distance: data.distance,
      json: data.json,
      job: data.job,
      images: images,
      university: data.university,
      company: data.company,
    });
    await user.save();
  } catch (e) {
    console.log(e);
  }
}
