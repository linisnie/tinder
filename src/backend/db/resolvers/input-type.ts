import { Field, InputType } from 'type-graphql';

@InputType()
export class SearchUserInput {
  @Field(() => String, { nullable: true })
  search?: string;

  @Field(() => String, { nullable: true })
  page?: string;

  @Field(() => String, { nullable: true })
  university?: string;

  @Field(() => String, { nullable: true })
  hasInst?: string;
}
