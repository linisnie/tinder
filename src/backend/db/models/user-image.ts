import {
  Entity,
  BaseEntity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Field, ID, ObjectType } from 'type-graphql';
import { Users } from './users';
import { ImageAttributes } from './image-attributes';

const NULLABLE = { nullable: true };

@Entity()
@ObjectType()
export class UserImage extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  idUserImage!: string;

  @Field(() => Users)
  @ManyToOne(() => Users, (user) => user.images)
  user!: Users;

  @Field(() => String)
  @Column('text')
  url!: string;

  @Field(() => String)
  @Column('text')
  fileName!: string;

  @Field(() => [ImageAttributes], NULLABLE)
  @OneToMany(
    () => ImageAttributes,
    (imageAttributes) => imageAttributes.image,
    NULLABLE,
  )
  attributes?: ImageAttributes[];

  @Field(() => String)
  @Column('timestamp', { default: () => 'CURRENT_TIMESTAMP' })
  createdAt!: string;
}
