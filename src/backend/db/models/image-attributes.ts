import {
  Entity,
  BaseEntity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
} from 'typeorm';
import { Field, ID, ObjectType, registerEnumType } from 'type-graphql';
import { UserImage } from './user-image';

export enum AttrNameTypes {
  STYLE = 'style',
  STYLE_CONFIDENCE = 'style_confidence',
}

registerEnumType(AttrNameTypes, {
  name: 'AttrNameTypes',
  description: 'Attribute name type',
});

@Entity()
@ObjectType()
export class ImageAttributes extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  idImageAttributes!: string;

  @Field(() => UserImage)
  @ManyToOne(() => UserImage, (userImage) => userImage.attributes)
  image!: UserImage;

  @Field(() => AttrNameTypes)
  @Column({
    type: 'enum',
    enum: AttrNameTypes,
  })
  name!: string;

  @Field(() => String)
  @Column('text')
  value!: string;

  @Field(() => String)
  @Column('timestamp', { default: () => 'CURRENT_TIMESTAMP' })
  createdAt!: string;
}
