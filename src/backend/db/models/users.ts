import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Field, ID, ObjectType } from 'type-graphql';
import { UserImage } from './user-image';

const NULLABLE = { nullable: true };

@Entity()
@ObjectType()
export class Users extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  idUser!: string;

  @Field(() => String)
  @Column('text')
  id!: string;

  @Field(() => String)
  @Column('text')
  bio!: string;

  @Field(() => String, NULLABLE)
  @Column('text', NULLABLE)
  job!: string;

  @Field(() => String, NULLABLE)
  @Column('text', NULLABLE)
  company!: string;

  @Field(() => String, NULLABLE)
  @Column('text', NULLABLE)
  university!: string;

  @Field(() => String, NULLABLE)
  @Column('text', NULLABLE)
  universityParsed!: string;

  @Field(() => String, NULLABLE)
  @Column('text', NULLABLE)
  instagram?: string;

  @Field(() => Number, NULLABLE)
  @Column('numeric', NULLABLE)
  height?: number;

  @Field(() => String)
  @Column('timestamp')
  birth!: string;

  @Field(() => String)
  @Column('text')
  name!: string;

  @Field(() => [UserImage], NULLABLE)
  @OneToMany(() => UserImage, (userImage) => userImage.user, NULLABLE)
  images?: UserImage[];

  @Field(() => Number)
  @Column('numeric')
  distance!: number;

  @Field(() => String)
  @Column('json')
  json!: { [key: string]: any };

  @Field(() => String)
  @Column('timestamp', { default: () => 'CURRENT_TIMESTAMP' })
  createdAt!: string;
}
