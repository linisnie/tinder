import { createConnection } from 'typeorm';
import { dislike, extractData, getData, like } from './parser/tinder-api';
import { dbParams, STRATEGY, LIMIT, DELAY } from './parser/constants';
import {uploadToDb} from "./db/resolvers/user";

(async () => {
  await createConnection(dbParams);
  await worker();
})();

let counter = 0;

async function worker() {
  const data = await getData();
  if (!data.data.results) {
    console.log('There are not profiles anymore, come back later!');
    return;
  }
  for (const user of data.data.results) {
    const extracted = extractData(user);
    let res;
    if (STRATEGY === 'like')
      res = await like(extracted.id, extracted.photoId, extracted.sNumber);
    else if (STRATEGY === 'dislike')
      res = await dislike(extracted.id, extracted.sNumber);

    if (res.status !== 200) {
      console.error('Blocked by tinder');
      return;
    }
    await uploadToDb(extracted);
    console.log(++counter);
    if (counter > LIMIT) return;
  }
  setTimeout(() => worker(), DELAY);
}
