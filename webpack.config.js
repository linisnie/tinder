const NODE_ENV = process.env.NODE_ENV || 'development';
require('@babel/polyfill');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
// process.traceDeprecation = true;
module.exports = {
  mode: NODE_ENV,
  entry: {
    js: ['@babel/polyfill', './src/frontend/app.tsx'],
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        styles: {
          name: 'styles',
          test: /\.css$/,
          chunks: 'all',
          enforce: true,
        },
      },
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/frontend/index.html',
      filename: __dirname + '/build/public/index.html',
    }),
    new webpack.NormalModuleReplacementPlugin(/type-graphql$/, (resource) => {
      resource.request = resource.request.replace(
        /type-graphql/,
        'type-graphql/dist/browser-shim.js',
      );
    }),
  ],
  module: {
    rules: [
      { test: /\.graphql?$/, loader: 'webpack-graphql-loader' },
      {
        test: /\.ts(x?)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'ts-loader',
          },
        ],
      },
      // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader',
      },
    ],
  },
  watch: NODE_ENV === 'development',
  watchOptions: {
    aggregateTimeout: 100,
    poll: true,
    ignored: /node_modules/,
  },
  resolve: {
    extensions: ['.ts', '.tsx', '*', '.js', '.jsx', '.graphql'],
  },
  output: {
    path: __dirname + '/build/public',
    publicPath: '/',
    filename: NODE_ENV === 'development' ? 'app.js' : 'app.[chunkhash:5].js',
  },
};
