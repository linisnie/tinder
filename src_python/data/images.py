import sys

sys.path.append('.')
sys.path.append('..')
sys.path.append('../../')

from configuration.path import paths
from data.database import Database
import pandas as pd
import requests
from tqdm import tqdm
import time
from utils.misc import get_cmd_arg


class ImageHandler():

    def __init__(self, photo_dir=paths.photo_dir):
        self.photo_dir = photo_dir
        self.db = Database()

    def load_image(self, fpath, image_url=None, pause_secs=0.1, replace=False):
        if fpath.exists() and not replace:
            return
        if image_url is None:
            try:
                query = "select url from user_image where file_name = %s;"
                image_url = self.db.query(query, (fpath.name,))[0][0]
            except Exception:
                raise Exception(f'Failed on query {query} with name {fpath.name}')

        time.sleep(pause_secs)
        img_data = requests.get(image_url).content
        with open(fpath, 'wb') as f:
            f.write(img_data)

    def save_all_images(self):
        df = self.db.get_sql_query_df(
            'select file_name, url from user_image order by created_at')

        for _, df_user in tqdm(df.iterrows(), total=len(df)):
            image_url = df_user['url']
            fpath = paths.photo_dir / df_user['file_name']
            self.load_image(fpath, image_url)


if __name__ == '__main__':
    # run with "python images.py -task save_images"
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)

    task = get_cmd_arg('-task', default='save_images', drop=True)

    if task == 'save_images':
        ImageHandler().save_all_images()
