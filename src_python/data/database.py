import pandas as pd
import psycopg2 as psycopg2

from configuration.path import main_dir
from utils.misc import read_json_file, get_config_attribute


def get_connection():
    conf_fpath = main_dir() / 'settings.json'
    conf = read_json_file(conf_fpath)
    db_params = get_config_attribute(conf, 'postgres')
    conn = psycopg2.connect(host=db_params['host'], database=db_params['database'],
                            user=db_params['username'],
                            password=db_params['password'])
    return conn


class Database(object):

    def __init__(self):
        self._conn = get_connection()
        self._cursor = self._conn.cursor()

    def query(self, query, params):
        self._cursor.execute(query, params)
        records = self._cursor.fetchall()
        return records

    def query_w_commit(self, query, params):
        self._cursor.execute(query, params)
        self._conn.commit()

    def insert_rows(self, query, params_tuple, verbose=False):
        # query = 'INSERT INTO image_attributes (name, value, image_id_user_image) VALUES '
        s = '(' + ','.join(['%s'] * len(params_tuple[0])) + ')'
        args_str = ','.join(
            self._cursor.mogrify(s, x).decode("utf-8") for x in params_tuple)
        self._cursor.execute(query + args_str)
        self._conn.commit()
        if verbose:
            print(f'query {query}... inserted {len(params_tuple)} columns')

    def get_sql_query_df(self, query):
        conn = get_connection()
        df = pd.read_sql_query(query, conn)
        conn.close()
        return df

    def __del__(self):
        self._conn.close()
