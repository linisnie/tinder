import os
from pathlib import Path
from types import SimpleNamespace


def main_dir() -> Path:
    py_fpath = Path(os.path.abspath(__file__))
    # py_fpath = Path('/Users/a1/JavaScript/projects/tinder/python/data/images.py')
    main_dir = py_fpath.parents[2]
    return main_dir


def photo_dir():
    photo_dir = main_dir() / 'data' / 'photo'
    if not photo_dir.exists():
        photo_dir.mkdir(parents=True)
    return photo_dir


paths = SimpleNamespace(main_dir=main_dir(),
                        data_dir=main_dir() / 'data',
                        photo_dir=photo_dir(),
                        model_dir=main_dir() / 'data' / 'models')
