from model.nsfw_model.nsfw_detector import predict
from configuration.path import paths
from tqdm import tqdm
import pandas as pd
import plotly.graph_objects as go


def run_model_and_fill_db():


    #
    # query = "ALTER TABLE mytable ADD COLUMN sid serial PRIMARY KEY"
    # cur = conn.cursor()
    # cur.execute(query)
    # conn.close()
    # conn.commit()
    pass

def analyse_sexuality():
    report_df_path = paths.data_dir / 'results' / f'sexy_report_{paths.photo_dir.name}.csv'
    report_df = pd.read_csv(report_df_path)
    report_df['id'] = report_df['file'].apply(lambda s: s.split('_')[0])
    result_grouped = report_df.groupby('id')['result'].value_counts().unstack().fillna(0).astype(int)
    result_grouped['sexy_ratio'] = (result_grouped['sexy'] + result_grouped['porn']) / result_grouped.sum(axis=1)
    result_grouped.sort_values('sexy_ratio', ascending=False)[:500]
    result_grouped[result_grouped.index=='5c2d297836f29a4b0acaf186']
    import plotly.express as px
    fig = px.histogram(result_grouped, x="sexy_ratio")
    fig.show(renderer='browser')

if __name__=='__main__':
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.width', 1000)
    run_model_and_fill_db()