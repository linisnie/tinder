import warnings

import pandas as pd
from tqdm import tqdm

from configuration.path import paths
from data.database import Database
from data.images import ImageHandler
from model.nsfw_model.nsfw_detector import predict


class Predictor():

    def __init__(self, h5_path, photo_dir=paths.photo_dir):
        self.model = predict.load_model(h5_path)
        self.photo_dir = photo_dir
        self.imageHandler = ImageHandler(photo_dir)
        self.db = Database()

    def predict_file(self, fpath: str):
        # preload image if does not exist
        self.imageHandler.load_image(fpath)
        try:
            out = predict.classify(self.model, fpath)
        except ValueError:
            out = {}
        return out

    def predict_file_w_url(self, fname, url):
        # preload image if does not exist
        fpath = self.photo_dir / fname
        self.imageHandler.load_image(fpath, url)
        try:
            out = predict.classify(self.model, fpath)
        except ValueError:
            warnings.warn(f'Model failed at {fpath.name}')
            out = {}
        return out

    def predict_files_w_url(self, files=None):
        """

        :param files: DataFrame
            Dataframe with columns 'file_name', 'url'
        :return:
        """
        predictions = []

        for _, file in tqdm(files.iterrows(), desc='make predictions', total=len(files)):
            out = self.predict_file_w_url(file['file_name'], file['url'])
            predictions.append(out)
        return predictions

    def predict_files_df(self, files=None):
        predictions = self.predict_files_w_url(files)
        predictions_df = pd.DataFrame(predictions)

        files.insert(files.shape[1], 'result', predictions_df.idxmax(axis=1))
        files.insert(files.shape[1], 'confidence', predictions_df.max(axis=1).round(3))
        # pd.concat([result_df, predictions_df], axis=1)
        return files


class DbPredictor(Predictor):

    def predict_sexy_write_to_db(self, name='style', verbose=False, bulk_size=500):
        images_df = self.db.get_sql_query_df('select * from user_image')

        # remove images, that already processed
        processed_df = self.db.get_sql_query_df('select * from image_attributes')
        processed_images = processed_df.loc[
            processed_df['name'] == name, 'image_id_user_image']
        processed_mask = images_df['id_user_image'].isin(processed_images)
        files = images_df.loc[~processed_mask, ['file_name', 'url', 'id_user_image']].reset_index(drop=True)
        if verbose:
            print(
                f'{processed_mask.sum()} predictions already in db, make predictions for {len(files)} images')
        bulk_starts = list(range(0, len(files)+1, bulk_size))
        for i, bulk_start in enumerate(bulk_starts):
            if verbose:
                print(f'bulk {i}/{len(bulk_starts)}')
                print()
            files_bulk = files[bulk_start:bulk_start+bulk_size]
            result_df = self.predict_files_df(files_bulk)

            values_style = result_df[['id_user_image', 'result']].copy()
            values_style.columns = ['id_user_image', 'value']
            values_style['name'] = name
            values_style_conf = result_df[['id_user_image', 'confidence']].copy()
            values_style_conf.columns = ['id_user_image', 'value']
            values_style_conf['name'] = f'{name}_confidence'
            values = pd.concat([values_style, values_style_conf]).sort_values('id_user_image')
            query = 'INSERT INTO image_attributes (image_id_user_image,value,name) VALUES '
            self.db.insert_rows(query, values.values, verbose=verbose)