import sys

sys.path.append('.')
sys.path.append('..')
sys.path.append('../../')

from model.predictor import DbPredictor
from utils.misc import get_cmd_arg
from configuration.path import paths
import pandas as pd


def copy_category_photos(result_df, photo_dir, category='sexy'):
    sexy_files = result_df.loc[result_df['result'] == category, 'file'].values
    from shutil import copyfile
    category_dir = paths.main_dir / 'data' / 'photo_by_category' / category
    category_dir.mkdir(exist_ok=True, parents=True)
    for fname in sexy_files:
        copyfile(photo_dir / fname, category_dir / fname)


def predict_image_style():
    h5_path = paths.model_dir / 'mobilenet_v2_140_224' / 'saved_model.h5'
    predictor = DbPredictor(h5_path)
    predictor.predict_sexy_write_to_db(verbose=True)

    # result_df.to_csv(paths.data_dir / 'results' / f'sexy_report_{photo_dir.name}.csv',
    #                  index=False)

    # for category in ['neutral', 'porn', 'sexy', 'hentai', 'drawings']:
    #     copy_category_photos(result_df, photo_dir, category)

if __name__ == '__main__':
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)

    task = get_cmd_arg('-task', default='image_style', drop=True)

    if task == 'image_style':
        predict_image_style()
