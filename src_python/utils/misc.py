import argparse
import sys
import numpy as np
import pandas as pd
import json
from types import SimpleNamespace


def read_json_file(path, type_=dict):
    with open(path, 'rb') as handle:
        config_dict = json.loads(handle.read())

    if type_ == dict:
        config = config_dict
    elif type_ == SimpleNamespace:
        config = SimpleNamespace(**config_dict)
    else:
        raise TypeError('wrong type_ argument')

    return config


def get_cmd_arg(name, type_=str, default=None, nargs=None, choices=None,
                drop=False, should_exist=False, **args):
    """
    Parse one argument from command line arguments.

    Arguments are similar to argparse.ArgumentParser().add_argument()

    Parameters
    ----------
    type_: type
        type of parsed variable, may be 'bool'
    drop: bool
        drop argument name and value from sys.argv variable
    should_exist: bool
        raise Error if argument is not found
    """

    init_type = None
    if type_ == bool:
        # change type to str for correct bool value parsing
        init_type = type_
        type_ = str

    if name.startswith('-'):
        # parse that argument type:  --dbname none
        parser = argparse.ArgumentParser()
        parser.add_argument(name, type=type_, default=default, choices=choices,
                            nargs=nargs, **args)
        args, _ = parser.parse_known_args()
        value = list(args.__dict__.values())[0]
    else:
        # parse that argument type: SAMPLER.train=same_ratio_batches
        name += '='
        arg_idx = \
            np.where([True if s.startswith(name) else False for s in sys.argv])[0]

        if len(arg_idx) == 0:
            value = default
        else:
            arg = sys.argv[arg_idx[0]]
            value = arg[len(name):]

            if drop:
                sys.argv.remove(arg)

    if drop:
        drop_args = np.where(np.array(sys.argv) == name)[0]

        if len(drop_args) > 0:
            assert len(drop_args) == 1, 'works only for one exclusive argument'

            if isinstance(value, list):
                drop_forward = len(value)
            else:
                drop_forward = 1

            drop_arg = drop_args[0]
            drop_args = np.arange(drop_arg, drop_arg + drop_forward + 1)
            drop_args = np.array(sys.argv)[drop_args]

            for a in drop_args:
                sys.argv.remove(a)

    if should_exist and default is None and value is None:
        err_msg = f'command line argument {name} is not defined'
        raise ValueError(err_msg)

    if value == 'None':
        value = None

    if init_type == bool and type(value) != bool:
        value = value.lower()
        if value in ['1', 'true']:
            value = True
        elif value in ['0', 'false']:
            value = False
        else:
            msg = f'boolean argument {name} has incorrect value {value}'
            raise ValueError(msg)
    return value


def get_config_attribute(conf, *argv, default=None, required=False):
    """
    Get value from config

    Parameters
    ----------
    conf : types.SimpleNamespace or dict
    default :
        default value returned if value was not found
    argv :
        name of item or sequence names for nested items

    :param required:
        raise AttributeError if path to parameter not found
    Returns
    -------
    conf :
        item value
    """

    if conf is None:
        return default

    if len(argv) == 0:
        # have reached value of attribute, so return it
        return conf

    nm = argv[0]

    if not isinstance(conf, dict):
        try:
            conf = conf.__dict__
        except AttributeError:
            if required:
                raise AttributeError(f'Required attribute "{nm}" not found')
            return None

    if nm not in conf:
        # No attribute, return default
        return default

    conf_next_level = conf[nm]
    return get_config_attribute(conf_next_level, *argv[1:],
                                default=default)

