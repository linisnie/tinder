## Установка

Скачать репозиторий

Выполнить 

`npm i`

чтобы запустить парсер, выполнить 

`npm start`

## Настройки

Настройки находятся в фалйе `settingss.josn`, который создаётся по шалонку файла `settings.template.json`

### Токены
Чтобы получить токены, нужно открыть веб версию тиндера, открыть инструменты разработчика, найти там запрос типа https://api.gotinder.com/updates?locale=en и посмотерть в его request headers

### Postgres
Нужно только создать бд, и указать её параметры в настройках

Добавить расширение

`CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`


### Python
Настройка окружения.
1) Где удобно, например в папке с приложением создаем виртуальное окружение `python3 -m venv env/tinder`
2) Активируем окружение `source env/tinder/bin/activate`
3) Устанавливаем зависимости `pip install -r src_python/requirements.txt`

Прогон модели. Определение сексуальных фоток. https://github.com/GantMan/nsfw_model. На выходе модели классы drawings, hentai, neutral, porn, sexy.
1) Сохраняем модель в папку data/models/mobilenet_v2_140_224 (Архив с ней тут https://github.com/GantMan/nsfw_model/releases/tag/1.1.0)
2) В папке src_python/model выполняем  `python run_model.py -task image_style`.
3) После прогона скрипта в таблице image_attributes добавятся аттрибуты 'style' (принимает значение одного из 5 классов или Null, если картинка битая) и 'style_confidence' (уверенность модели). Фотки будут сохраняться в data/photo.